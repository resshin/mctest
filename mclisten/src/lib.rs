use std::net::{Ipv4Addr, UdpSocket};
use std::str;

pub fn start_listening(ip_address: &str, port: &u16) {
  let socket = UdpSocket::bind(("0.0.0.0", *port)).unwrap();
  let mut buff = [0u8; 65535];
  let multicast_address: Ipv4Addr = ip_address.parse().unwrap();

  println!("Joining multicast: {}:{}", ip_address, port);
  socket.join_multicast_v4(&multicast_address, &Ipv4Addr::UNSPECIFIED)
      .expect("Unable to join multicast group");

  loop {
    let (len, src) = socket.recv_from(&mut buff).unwrap();
    println!("Received {} bytes from {}", len, src);
  }
}
