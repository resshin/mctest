use std::net::{Ipv4Addr, UdpSocket};
use std::str;
use std::thread;
use std::time::Duration;

use lipsum::lipsum_words_from_seed;

pub fn start_publishing(ip_address: &str, port: &u16) {
  let socket = UdpSocket::bind(("0.0.0.0", *port)).unwrap();
  let multicast_address: Ipv4Addr = ip_address.parse().unwrap();

  println!("Joining multicast: {}:{}", ip_address, port);
  socket.join_multicast_v4(&multicast_address, &Ipv4Addr::UNSPECIFIED)
      .expect("Unable to join multicast group");

  let mut i: u64 = 0;
  loop {
    let random_words = lipsum_words_from_seed(10, i);

    println!("Sending: {}", random_words);
    socket.send_to(random_words.as_bytes(), (multicast_address, *port))
        .expect("Unable to send message");
    thread::sleep(Duration::from_millis(500));
    i = i + 1;
  }
}
