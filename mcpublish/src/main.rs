use clap::{App, Arg};
use mcpublish::start_publishing;

fn main() {
  let matches = App::new("Multicast Publisher")
      .version("0.1.0")
      .author("Daniel Albert <daniel.albert.art@gmail.com>")
      .about("Publish messages on a IP multicast")
      .arg(Arg::with_name("ip_address")
          .help("IP address")
          .required(true)
          .index(1))
      .arg(Arg::with_name("port")
          .help("Port")
          .required(true)
          .index(2))
      .get_matches();

  let ip_address = matches.value_of("ip_address").unwrap();
  let port = matches.value_of("port")
      .map(|s| s.parse::<u16>())
      .unwrap()
      .unwrap();

  start_publishing(&ip_address, &port);
}
